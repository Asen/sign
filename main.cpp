#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "LogicStructures/openGL/glut.h"
#include "LogicStructures/math/math.h"
#include "LogicStructures/BaseSysInfo/BaseSysInfo.h"
#include "LogicStructures/Point/Point.h"
#include "LogicStructures/Figure/Figure.h"
#include "LogicStructures/Exception/Exception.h"
#include "LogicStructures/Recognizer/Recognizer.h"
using namespace std;

class GLUT{
    public:
        GLUT(int argc, char* argv[]);
        static void    Display(void);
    private:
        void           Init(int argc, char* argv[]);
        static void    ResizeWnd(int,int);
        static void    Timer(int);
        static void    Motion(int, int);
        static void    Mouse(int, int, int, int);
} *glut;

BaseSysInfo *si         =  new BaseSysInfo();
LogicFigure *collection =  new LogicFigure();
Recognizer  *rec        =  new Recognizer();
static GLfloat X, Y;

GLUT::GLUT(int argc, char* argv[])
{
    Init(argc,argv);
}

void GLUT::Display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_LINE_STRIP);
    glColor3fv(si->drawColor);
    for(int i=0;i<collection->vertex.size();i++)
    {
        if(collection->vertex[i].x>0 && collection->vertex[i].y>0)
        glVertex2f(collection->vertex[i].x,collection->vertex[i].y);
    }
    if(collection->vertex.size()>0)
    if(si->LOGIC_MODE==-1) glVertex2d(X,Y);
    glEnd();

    glutSwapBuffers();
}

void GLUT::ResizeWnd(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,1,0,1,0,1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

void GLUT::Timer(int)
{
    static int timer = 0;
    static int last = -1;
    if(si->LOGIC_MODE!=-1 && collection->vertex.size()>0)
    {
        if(timer>=300) {
                rec->ShowRecognition();
                collection->clear();
                timer=0;
                }
        if(collection->vertex.size()==last) timer++; else timer=0;
        last = collection->vertex.size();
    }
    glut->Display();
    glutTimerFunc(10,Timer,0);
}

void GLUT::Motion(int x, int y)
{
        double xx = (double)x/si->WIN_SIZE;
        double yy = 1-(double)y/si->WIN_SIZE;
        X=xx;
        Y=yy;
        if(si->LOGIC_MODE==-1){ glut->Display(); return; }

        if(collection->vertex.size()>0 && getDistance(collection->vertex[collection->vertex.size()-1],Point(xx,yy))<0.01) return;
        collection->vertex.push_back(Point(xx,yy));

        rec->Recognize(collection);
}


void GLUT::Mouse(int button, int state, int x, int y)
{
    if(button==GLUT_MIDDLE_BUTTON && state==GLUT_DOWN)
    {
        if(collection->name=="") { cout<<"Enter a name."<<endl; return; }

        ofstream f(si->fileName.c_str(),ios::out | ios::app);
        f<<endl<<" "<<si->sep<<"\n "<<collection->name<<" ";
        f<<countAngle( collection->vertex[ collection->vertex.size()-1 ], collection->vertex[0], collection->vertex[1] )<<" ";
        for(int i=2;i<collection->vertex.size();i++)
            f << countAngle( collection->vertex[i-2], collection->vertex[i-1], collection->vertex[i] ) << " ";

        f <<" "<< (double)getDistance( collection->vertex[1], collection->vertex[0] ) /
        getDistance( collection->vertex[0], collection->vertex[collection->vertex.size()-1] ) << " ";
        for(int i=2;i<collection->vertex.size();i++)
            f << (double)getDistance( collection->vertex[i], collection->vertex[i-1] ) / getDistance( collection->vertex[i-1], collection->vertex[i-2] ) << " ";

        f.close();
        si->LOGIC_MODE=1;
        cout<<"Done!"<<endl;
    }

    if(button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN)
    {
        if(si->LOGIC_MODE==-1) return;
        si->LOGIC_MODE*=-1;
        collection->vertex.clear();
        if(si->LOGIC_MODE==-1) { cout<<"Name: "; cin>>collection->name; }
    }

    if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
    {
        double xx = (double)x/si->WIN_SIZE;
        double yy = 1-(double)y/si->WIN_SIZE;
        if(si->LOGIC_MODE==-1) collection->vertex.push_back(Point(xx,yy));
    }

}

void GLUT::Init(int argc, char* argv[])
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(si->WIN_SIZE,si->WIN_SIZE);
    glutInitWindowPosition(0,0);
    glutCreateWindow(si->caption.c_str());
    glClearColor(si->backColor[0],si->backColor[1],si->backColor[2],1.0);
    glEnable(GL_DEPTH_TEST | GL_NORMALIZE);

    glutDisplayFunc(this->Display);
    glutReshapeFunc(this->ResizeWnd);
    glutTimerFunc(10,this->Timer,0);
    glutMotionFunc(this->Motion);
    glutMouseFunc(this->Mouse);

    glutMainLoop();
}

void unCaughtException()
{
    exit(0);
}

int main(int argc, char* argv[])
{
    set_terminate( unCaughtException );

    try{
        rec->LoadShapeBase();
        glut = new GLUT(argc, argv);
        }catch(Exception e){ cout<<e.getMessage()<<endl; }

    return ERROR_SUCCESS;
}

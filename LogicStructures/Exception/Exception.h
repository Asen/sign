#ifndef EXCEPTION_H
#define EXCEPTION_H
#include<string>

class Exception
{
    public:
        Exception(std::string msg);
        virtual ~Exception();
        std::string getMessage();

    private:
        std::string message;
};



#endif // EXCEPTION_H

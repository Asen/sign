#ifndef MATH_H
#define MATH_H
#include "../Point/Point.h"

int countAngle(Point _a, Point _b, Point _c);
double getDistance(const Point p1, const Point p2);
double _abs(double dig);
int _angle(const double COS);

#endif // MATH_H
